import unittest

import mock
from use_mock.person import Person

class TestPerson(unittest.TestCase):
    def setUp(self):
        # Instantiate a new Person object for every tests below
        self.myPerson = Person()

    @mock.patch('use_mock.person.sys.stdout')
    def test_speak(self, mockStdout):
        #  Test speak
        self.myPerson.speak('haha')

        # Ensure stdout.write has been called with haha
        mockStdout.write.assert_has_calls([
            mock.call('haha\n')
        ])

        # Reset mock object
        mockStdout.reset_mock()

        #  Test speak
        self.myPerson.speak('hehe')

        # Ensure stdout.write has been called with hehe
        mockStdout.write.assert_has_calls([
            mock.call('hehe\n')
        ])

    @mock.patch('use_mock.person.sys')
    def test_yell(self, mockSys):
        self.myPerson.yell('haha')
        mockSys.stdout.write.assert_has_calls([
            mock.call('haha\n')
        ])
        mockSys.stderr.write.assert_has_calls([
            mock.call('haha!\n')
        ])