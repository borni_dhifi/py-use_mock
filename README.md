Use Mock
===========

[![build status](https://gitlab.com/mondwan/py-use_mock/badges/master/build.svg)](https://gitlab.com/mondwan/py-use_mock/commits/master)
[![coverage](https://gitlab.com/mondwan/py-use_mock/badges/master/coverage.svg?job=coverage)](https://mondwan.gitlab.io/py-use_mock/coverage)

Demonstrate how to use [mock][1] to mock out stdout and stderr for unit test. For detail, please head to my [blog][2].

@last-modified: 30 MAY 2017

[1]: https://docs.python.org/3/library/unittest.mock.html#module-unittest.mock
[2]: http://mondwan.blogspot.com/2017/05/unit-test-on-python-how-to-mock-out.html
